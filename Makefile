# Copyright (C) 2015-2018 Ola Nilsson <ola.nilsson@gmail.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

EFLAGS = -Q -L .
EFLAGS += -L packages

ERT_JUNIT_BRANCH ?= master

ELS = ert-junit-reference-tests.el
elcs = $(ELS:.el=.elc)
EMACS ?= emacs
loadfiles = $(addprefix -l ,$(packagefiles) $(elcs)) $(EXTRA_LOADFILES)

ever:=$(word 3,$(shell $(EMACS) --version | head -1))
ever_major=$(firstword $(subst ., ,$(ever)))

packages_23 = https://raw.githubusercontent.com/ohler/ert/fb3c278d/lisp/emacs-lisp/ert.el
packages = $(packages_$(ever_major)) $(EXTRA_PACKAGES)
packages += https://bitbucket.org/olanilsson/ert-junit/raw/$(ERT_JUNIT_BRANCH)/ert-junit.el

CURL = curl -fsSkL --create-dirs --retry 9 --retry-delay 9

circleci_junit = $(if $(CIRCLECI),$(CIRCLE_WORKING_DIRECTORY)/test_results/test.xml)
shippable_junit = $(if $(SHIPPABLE),shippable/testresults/tests.xml)
pipelines_junit = $(if $(BITBUCKET_BUILD_NUMBER),test-results/junit.xml)

JUNIT_OUT ?= $(or $(circleci_junit),$(shippable_junit),$(pipelines_junit),junit.xml)
$(if $(word 2,$(JUNIT_OUT)),$(error JUNIT_OUT may only contain one filename, was "$(JUNIT_OUT)"))

all: lisp

lisp: packages $(elcs)


$(info MAKEFILE_LIST: $(MAKEFILE_LIST))

%.elc: %.el
	$(EMACS) --batch $(EFLAGS) -f batch-byte-compile $^

$(foreach p,$(packages),\
	$(eval packages/$(notdir $(p)): ; $(CURL) $p -o $$@))

packagefiles = $(addprefix packages/,$(notdir $(packages)))

packages: $(packagefiles)

#run tests
check test: lisp packages
	mkdir -p $(dir $(JUNIT_OUT))
	$(EMACS) --batch $(EFLAGS) $(loadfiles) -f ert-junit-run-tests-batch-and-exit $(JUNIT_OUT) || true

lisp-clean:
	-rm -rf *.elc test/*.elc

clean: lisp-clean
	-rm -rf  packages

.PHONY: all check test lisp clean packages coverage

