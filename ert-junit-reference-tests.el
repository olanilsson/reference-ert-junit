;;; ert-junit-reference-tests.el --- Various ert test results -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A set of ERT testcases that can be used to evaulate test runners.

;;; Code:

(require 'ert)

(ert-deftest ok-no-output ()
  "A simple passing test with no output."
  t)

(ert-deftest skip-unless ()
  (skip-unless nil)
  t)

(ert-deftest skipped ()
  (ert-skip '(foo bar baz)))

(ert-deftest skip-with-string ()
  (ert-skip "skipstring"))

(ert-deftest unexpected-ok ()
  :expected-result :failed
  t)

(ert-deftest expected-fail ()
  :expected-result :failed
  (should (= 1 2)))

(ert-deftest fail-no-output ()
  "A simple failing test with no output."
  (should (= 1 2)))

(ert-deftest error-no-output ()
  "A simple erroring test with no output."
  (error "This is an expected error"))

(ert-deftest ok-with-info ()
  (ert-info ("info")
	t))

(ert-deftest skipped-with-info ()
  (ert-info ("info")
	(skip-unless nil)
	t))

(ert-deftest unexpected-ok-with-info ()
  :expected-result :failed
  (ert-info ("info")
	t))

(ert-deftest expected-fail-with-info ()
  :expected-result :failed
  (ert-info ("info")
	(should (= 1 2))))

(ert-deftest fail-with-info ()
  (ert-info ("info")
	(should (= 1 2))))

(ert-deftest error-with-info ()
  (ert-info ("info")
	(error "This is an expected error")))

(ert-deftest quitting ()
  (ert-info ("quitting is such sweet sorrow")
	(signal 'quit nil)))

(provide 'ert-junit-reference-tests)
;;; ert-junit-reference-tests.el ends here
